#!/bin/sh
file="counter.txt"
touch "$file"

echo "File: $file"

counter=`tail "$file"`
echo "Current count: $counter"

echo "wait for input of any key"
while true;
do
    read -rsn1 input
	if [ "$input" ] || [ -z "$input" ];
	then
		counter=$((counter + 1))
		echo "$counter" > "$file"
		printf "$counter | "
	fi
done
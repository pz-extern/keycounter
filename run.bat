@echo off

set file=counter.txt

if not exist %file% (
  (echo 0)> %file%
)

set/p count=<%file%

echo File: %file%
echo Current count: %count%
echo Exit with STRG+C

echo ---------------------------------

echo wait for input of any key
echo|set /p="%count% "

:input
pause>nul

:increase
set/a count+=1
(echo %count%)> %file%
echo|set /p=" | %count% "
goto input